<?php
    namespace App\Core\Role;

    #Klasa za proveru da li korisnik ima sesiju,i ako ne da ga redirektuje na user/login.
    class UserRoleController extends \App\Core\Controller {
        public function __pre() {
            if ($this->getSession()->get('user_id') === null) {
                $this->redirect('/user/login');
            }
        }
    }
