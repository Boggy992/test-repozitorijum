<?php
    namespace App\Controllers;

    use App\Models\SpeciesModel;

    class MainController extends \App\Core\Controller {
          
        public function home() {
            $speciesModel = new SpeciesModel($this->getDatabaseConnection());
            $specieses = $speciesModel->getAll();
            $this->set('specieses', $specieses);

        }    

          /*  $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $adModel->add([
                'name'                         => 'Poklanjamo ljubicastog kameleona',
                'race_id'                      => '3',
                'user_id'                      => '1',     
                'is_documentation_available'   =>  1, 
                'is_vaccinated'                =>  0,     
                'age'                          => '2', 
                'hair_skin_colour'             => 'Ljubicast',  
                'weight'                       => '0.2',
                'description'                  => 'ljubicasti kameleon slatki ljubicasti kameleon ljubicasti kameleon slatki ljubicasti kameleon' 
            ]); */

    

           /* $staraVrednost = $this->getSession()->get('brojac', 0);
            $novaVrednost = $staraVrednost + 1;
            $this->getSession()->put('brojac', $novaVrednost);
            $this->set('podatak', $staraVrednost); */

        public function getRegister() {
            #...
        }
            #cemu dupla provera sa filter_sanitize_email kad vec u htmlu ima type="email"?
        public function postRegister() {
            $email     = \filter_input(INPUT_POST, 'reg_email'     , FILTER_SANITIZE_EMAIL);
            $forename  = \filter_input(INPUT_POST, 'reg_forename'  , FILTER_SANITIZE_STRING);
            $surname   = \filter_input(INPUT_POST, 'reg_surname'   , FILTER_SANITIZE_STRING);
            $username  = \filter_input(INPUT_POST, 'reg_username'  , FILTER_SANITIZE_STRING);
            $password1 = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2 = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);
   

            if ($password1 !== $password2) {
                $this->set('message', 'Doslo je do greske: Niste uneli dva puta istu lozinku!');
                return;
            }
            
            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(120)
                ->isValid($password1);

            if (!$validanPassword) {
                $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return;
            }   
                
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $user = $userModel->getByFieldName('email', $email);
            if ($user) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tim e-mailom.');
                return;
            }
            

            $user = $userModel->getByFieldName('username', $username);
            if ($user) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tim korisnickim imenom.');
                return;
            }

            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $userId = $userModel->add([
                'username'      => $username,
                'password_hash' => $passwordHash,
                'email'         => $email,
                'forename'      => $forename,
                'surname'       => $surname,
            ]);

            if (!$userId) {
                $this->set('message', 'Doslo je do greske: Nije bilo uspesno registrovanje naloga.');
                return;
            }

            $this->set('message', 'Napravljen je novi nalog.Sada mozete da se prijavite!');

        }

        public function getLogin() {

        }
            
        public function postLogin() {
            $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(120)
                ->isValid($password);

            if (!$validanPassword) {
                $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return;
            }

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $user = $userModel->getByFieldName('username', $username);
            if (!$user) {
                $this->set('message', 'Doslo je do greske: Ne postoji korisnik sa tim korisnickim imenom.');
                return;
            }

            if (!password_verify($password, $user->password_hash)) {
                sleep(1);
                $this->set('message', 'Doslo je do greske: Lozinka nije ispravna.');
                return;
            }

            $this->getSession()->put('user_id', $user->user_id);
            $this->getSession()->save();

            $this->redirect('/user/profile');
        }
    }