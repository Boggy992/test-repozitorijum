<?php
    namespace App\Controllers;

    class UserAdManagementController extends \App\Core\Role\UserRoleController {
        public function ads() {
            $userId = $this->getSession()->get('user_id');

            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $ads = $adModel->getAllByUserId($userId);
            $this->set('ads', $ads);
        }

        public function getAdd() {
            $raceModel = new \App\Models\RaceModel($this->getDatabaseConnection());
            $races = $raceModel->getAll();
            $this->set('races', $races);
        }

        public function postAdd() {
            $addData = [
                'name'                       => \filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING),
                'description'                => \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING),
                'age'                        => \filter_input(INPUT_POST, 'age', FILTER_SANITIZE_NUMBER_INT),
                'weight'                     => sprintf("%.1f", \filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_FLOAT)),
                'hair_skin_colour'           => \filter_input(INPUT_POST, 'hair_skin_colour', FILTER_SANITIZE_STRING),
                'is_documentation_available' => \filter_input(INPUT_POST, 'is_documentation_available') ?? 0 ? 1 : 0,
                'is_vaccinated'              => \filter_input(INPUT_POST, 'is_vaccinated') ?? 0 ? 1 : 0,
                'race_id'                    => \filter_input(INPUT_POST, 'race_id', FILTER_SANITIZE_NUMBER_INT),
                'user_id'                    => $this->getSession()->get('user_id')
            ];

            
            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());

            $adId = $adModel->add($addData);

            if (!$adId) {
                $this->set('message', 'Nije dodat oglas.');
                return;
            }

            $uploadStatus = $this->doPictureUpload('picture', $adId);
            if (!$uploadStatus) {
                $this->set('message', 'Oglas je dodat ali nije dodata slika!');
                return;
            }

            $this->redirect('/user/ads');
        }

        public function getEdit($adId) {
            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $ad = $adModel->getById($adId);

            if (!$ad) {
                $this->redirect('/user/ads');
                return;
            }

            if ($ad->user_id != $this->getSession()->get('user_id')) {
                $this->redirect('/user/ads');
                return;
            }

            $this->set('ad', $ad);

            $raceModel = new \App\Models\RaceModel($this->getDatabaseConnection());
            $races = $raceModel->getAll();
            $this->set('races', $races);
        }

        public function postEdit($adId) {
            $this->getEdit($adId);

            $editData = [
                'name'                       => \filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING),
                'description'                => \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING),
                'age'                        => \filter_input(INPUT_POST, 'age', FILTER_SANITIZE_NUMBER_INT),
                'weight'                     => sprintf("%.1f", \filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_FLOAT)),
                'hair_skin_colour'           => \filter_input(INPUT_POST, 'hair_skin_colour', FILTER_SANITIZE_STRING),
                'is_documentation_available' => \filter_input(INPUT_POST, 'is_documentation_available') ?? 0 ?: 0,
                'is_vaccinated'              => \filter_input(INPUT_POST, 'is_vaccinated') ?? 0 ?: 0,
                'race_id'                    => \filter_input(INPUT_POST, 'race_id', FILTER_SANITIZE_NUMBER_INT),
            ];

            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());

            $res = $adModel->editById($adId, $editData);

            if (!$res) {
                $this->set('message', 'Nije moguce izmeniti oglas!');
                return;
            }

            $uploadStatus = $this->doPictureUpload('picture', $adId);
            if (!$uploadStatus) {
                # $this->set('message', 'Oglas je izmenjen ali nije dodata nova slika!');
                return;
            }

            $this->redirect("/user/ads/");
        }

        private function doPictureUpload(string $fieldName, string $adId): bool {
            $pictureModel = new \App\Models\PictureModel($this->getDatabaseConnection());
            $picture = $pictureModel->getAllByAdId(intval($adId));

            \unlink(\Configuration::UPLOAD_DIR . $picture->picture_path);

            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($adId);
            $file->addValidations([ 
                new \Upload\Validation\Mimetype("image/jpeg"),
                new \Upload\Validation\Size("2M")
            ]);

            try {
                $file->upload();

                $fullFilename = $file->getNameWithExtension();

                $pictureModel->editById(intval($adId), [
                    'picture_path' => $fullFilename
                ]);
                

                #ovde
                return true;
            } catch (\Exception $e) {
                $this->set('message', 'Greska:   ' .$e->getMessage());
                return false;
            }
        }
    }