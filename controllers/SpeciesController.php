<?php
    namespace App\Controllers;

    use App\Models\SpeciesModel;
    use App\Models\RaceModel;


    class SpeciesController extends \App\Core\Controller {
           
           public function show($id) {
           
           $speciesModel = new SpeciesModel($this->getDatabaseConnection());
           $species = $speciesModel->getById($id);

            if ( !$species ) {
                header ('Location: /');
                exit;
            }

           $this->set('species', $species);

            
           $raceModel = new RaceModel($this->getDatabaseConnection());
           $racesInSpecies = $raceModel->getAllBySpeciesId($id);
           $this->set('racesInSpecies', $racesInSpecies);
    
          
        }

    }