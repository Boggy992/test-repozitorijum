<?php
    namespace App\Controllers;

    
    use App\Models\RaceModel;
    use App\Models\AdModel;


    class RaceController extends \App\Core\Controller {
           
           public function show($id) {
            
           $raceModel = new RaceModel($this->getDatabaseConnection());
           $race = $raceModel->getById($id);

            if ( !$race) {
                header('Location: /');
                exit;
            }

           $this->set('race', $race);

           $adModel = new AdModel($this->getDatabaseConnection());
           $adsInRaces = $adModel->getAllByRaceId($id);
           $this->set('adsInRaces', $adsInRaces);
    
          
        }

    }