<?php
    namespace App\Controllers;


    class UserRaceManagementController extends \App\Core\Role\UserRoleController {
        public function races() {
            $raceModel = new \App\Models\RaceModel($this->getDatabaseConnection());
            $races = $raceModel->getAll();
            $this->set('races', $races);

        }

        public function getEdit($raceId) {
            $raceModel = new \App\Models\RaceModel($this->getDatabaseConnection());
            $race = $raceModel->getById($raceId);

            if (!$race) {
                $this->redirect('/user/races');
            }

            $this->set('race', $race);

            return $raceModel;
        }

        public function postEdit($raceId) {
            $raceModel = $this->getEdit($raceId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $raceModel->editById($raceId, [
                'name' => $name
            ]);

            $this->redirect('/user/races');
        }

        public function getAdd() {

        }

        public function postAdd() {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $raceModel = new \App\Models\RaceModel($this->getDatabaseConnection());

            $raceId = $raceModel->add([
                'name' => $name
            ]);

            if ($raceId) {
                $this->redirect('/user/races');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovu rasu');
        }
    }