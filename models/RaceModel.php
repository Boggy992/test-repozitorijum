<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    

    class RaceModel extends Model{
        protected function getFields(): array {
            return [
                'race_id'    => new Field ((new NumberValidator())->setIntegerLength(10), false),
                'name'       => new Field ((new StringValidator())->setMinLength(1)->setMaxLength(100)),
                'species_id' => new Field ((new NumberValidator())->setIntegerLength(10))
                
            ]; 
        }
    
         public function getAllBySpeciesId(int $speciesId): array {
             return $this->getAllByFieldName('species_id', $speciesId);
           

    }


        
}