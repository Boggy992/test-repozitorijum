<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class SpeciesModel extends Model {
        protected function getFields(): array {
            return [
                'species_id' => new Field ((new NumberValidator())->setIntegerLength(10), false),
                'name'       => new Field ((new StringValidator())->setMinLength(1)->setMaxLength(100))
            ]; 
        }
        
    }