<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class PictureModel extends Model {
        protected function getFields(): array {
            return [
                'picture_id'   => new Field ((new NumberValidator())->setIntegerLength(10), false),
                'picture_path' => new Field ((new StringValidator())->setMinLength(1)->setMaxLength(255)),
                'user_id'      => new Field ((new NumberValidator())->setIntegerLength(10)),
                'ad_id'        => new Field ((new NumberValidator())->setIntegerLength(10))
                
            ]; 
        }

        public function getAllByAdId(int $adId): array {
            return $this->getAllByFieldName('ad_id', $adId);
           
        }
        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
           
        }
        #u kontroleru dodaje se 8 zapisa za slike.
    
    }
            