<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\BitValidator;

    class AdModel extends Model {
        protected function getFields(): array {
            return [
                'ad_id'                      => new Field ((new NumberValidator())  ->setIntegerLength(10), false),
                'name'                       => new Field ((new StringValidator())  ->setMaxLength(255)),
                'created_at'                 => new Field ((new DateTimeValidator())->allowDate()->allowTime(), false),
                'race_id'                    => new Field ((new NumberValidator())  ->setIntegerLength(10)),
                'user_id'                    => new Field ((new NumberValidator())  ->setIntegerLength(10)),
                'is_documentation_available' => new Field  (new BitValidator()),
                'is_vaccinated'              => new Field  (new BitValidator()),
                'age'                        => new Field ((new NumberValidator())  ->setIntegerLength(10)),
                'hair_skin_colour'           => new Field ((new StringValidator())  ->setMaxLength(255)),  
                'weight'                     => new Field ((new NumberValidator())  ->setDecimal()->setUnsigned()->setIntegerLength(4)->setMaxDecimalDigits(1)),  
                'description'                => new Field ((new StringValidator())  ->setMinLength(50)->setMaxLength(800))    
                
            ]; 
        }

        public function getAllByRaceId(int $raceId): array {
            return $this->getAllByFieldName('race_id', $raceId);
           
    }

        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
       
    }
    
        public function getAllBySearch(string $keywords) {
           $sql = 'SELECT * FROM `ad` WHERE `name` LIKE ? OR `description` LIKE ?;';
           
           $keywords = '%' . $keywords . '%';

           $prep = $this->getConnection()->prepare($sql);
           if (!$prep) {
               return [];
           }

           $res = $prep->execute([$keywords, $keywords]);
           if (!$res) {
               return [];
           }

           return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        
}