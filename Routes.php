<?php
    return [                    #pattern            #controller  #method
        App\Core\Route::get('|^user/register/?$|',  'Main',   'getRegister'),
        App\Core\Route::post('|^user/register/?$|', 'Main',   'postRegister'),
        App\Core\Route::get('|^user/login/?$|'    , 'Main',   'getLogin'),
        App\Core\Route::post('|^user/login/?$|'   , 'Main',   'postLogin'),

        App\Core\Route::get('|^species/([0-9]+)/?$|', 'Species', 'show'),
        
        App\Core\Route::get('|^race/([0-9]+)/?$|', 'Race', 'show'),
        
        App\Core\Route::get('|^ad/([0-9]+)/?$|', 'Ad', 'show'),
        App\Core\Route::post('|^search/?$|', 'Ad', 'postSearch'),

        # API rute:
        App\Core\Route::get('|^api/ad/([0-9]+)/?$|', 'ApiAd',  'show'),

        # User role routes:
        App\Core\Route::get('|^user/profile/?$|', 'UserDashboard', 'index'),

        App\Core\Route::get('|^user/races/?$|', 'UserRaceManagement', 'races'),
        App\Core\Route::get('|^user/races/edit/([0-9]+)/?$|', 'UserRaceManagement', 'getEdit'),
        App\Core\Route::post('|^user/races/edit/([0-9]+)/?$|', 'UserRaceManagement', 'postEdit'),
        App\Core\Route::get('|^user/races/add/?$|', 'UserRaceManagement', 'getAdd'),
        App\Core\Route::post('|^user/races/add/?$|', 'UserRaceManagement', 'postAdd'),

        App\Core\Route::get('|^user/ads/?$|', 'UserAdManagement', 'ads'),
        App\Core\Route::get('|^user/ads/edit/([0-9]+)/?$|', 'UserAdManagement', 'getEdit'),
        App\Core\Route::post('|^user/ads/edit/([0-9]+)/?$|', 'UserAdManagement', 'postEdit'),
        App\Core\Route::get('|^user/ads/add/?$|', 'UserAdManagement', 'getAdd'),
        App\Core\Route::post('|^user/ads/add/?$|', 'UserAdManagement', 'postAdd'),

        App\Core\Route::any('|^.*$|', 'Main', 'home')
    ];