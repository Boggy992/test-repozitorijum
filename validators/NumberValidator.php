<?php
    namespace App\Validators;

    use App\Core\Validator;

    class NumberValidator implements Validator {

        # da li ima mogucnost negativnih brojeva ili ne(boolean vrednost)
        private $isSigned;

        # koliko ima cifara u celom delu broja
        private $integerLength;

        # da li je broj realan ili ne(boolean vrednost)
        private $isReal;

        # maksimalan broj decimalnih cifara
        private $maxDecimalDigits;

        # pocetno i podrazumevano postavljanje parametara
        public function __construct() {
            $this->isSigned         = false;
            $this->isReal           = false;
            $this->integerLength    = 10;
            $this->maxDecimalDigits = 2;

        }

        public function &setInteger() : NumberValidator {
            $this->isReal = false;
            return $this;
        }

        public function &setDecimal() : NumberValidator {
            $this->isReal = true;
            return $this;
        }

        public function &setSigned() : NumberValidator {
            $this->$isSigned = true;
            return $this;
        }

        public function &setUnsigned() : NumberValidator {
            $this->isSigned = false;
            return $this;
        }

        public function &setIntegerLength(int $length) : NumberValidator {
            $this->integerLength = max(1, $length);
            return $this;
        }

        public function &setMaxDecimalDigits(int $maxDigits) : NumberValidator {
            $this->maxDecimalDigits = max(2, $maxDigits);
            return $this;
        }


        public function isValid(string $value): bool {
            $pattern = '/^';

            if ($this->isSigned === true) {
                $pattern .= '\-?';
            }
                        # moguce da ovde treba nesto da se doda zbog "weight" polja
            $pattern .= '[0-9]{0,' . ($this->integerLength-1) . '}';

            if ($this->isReal === true) {
                $pattern .= '\.[0-9]{0,' . $this->maxDecimalDigits . '}';
            }

            $pattern .= '$/';

            return \boolval(\preg_match($pattern, $value));
        }
    }